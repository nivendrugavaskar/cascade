//
//  Custombarbutton.m
//  CarmaApp
//
//  Created by Nivendru Gavaskar on 06/08/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import "Custombarbutton.h"



@implementation Custombarbutton


+(UIButton *)setbarbutton
{
    UIButton *custombutton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
   // [custombutton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    custombutton.backgroundColor=[UIColor clearColor];
    UIImageView *buttonimageview=[[UIImageView alloc]initWithFrame:CGRectMake(5, 5, 20, 20)];
    buttonimageview.image=[UIImage imageNamed:@"arrow_icon.png"];
    buttonimageview.contentMode=UIViewContentModeScaleAspectFit;
    [custombutton addSubview:buttonimageview];
    return custombutton;
}

+(UIButton *)setcartbutton
{
    UIButton *custombutton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    // [custombutton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    custombutton.backgroundColor=[UIColor clearColor];
    UIImageView *buttonimageview=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    buttonimageview.image=[UIImage imageNamed:@"cart_icon.png"];
    buttonimageview.contentMode=UIViewContentModeScaleAspectFit;
    
    
    
    
    [custombutton addSubview:buttonimageview];
    return custombutton;
}

+(UIButton *)setmenubarbutton
{
    UIButton *custombutton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    // [custombutton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    custombutton.backgroundColor=[UIColor clearColor];
    UIImageView *buttonimageview=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    buttonimageview.image=[UIImage imageNamed:@"menu_icon.png"];
    buttonimageview.contentMode=UIViewContentModeScaleAspectFit;
    [custombutton addSubview:buttonimageview];
    return custombutton;
}
+(UIButton *)setsearchbarbutton
{
    UIButton *custombutton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    // [custombutton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    custombutton.backgroundColor=[UIColor clearColor];
    UIImageView *buttonimageview=[[UIImageView alloc]initWithFrame:CGRectMake(5, 5, 20, 20)];
    buttonimageview.image=[UIImage imageNamed:@"search_icon.png"];
    buttonimageview.contentMode=UIViewContentModeScaleAspectFit;
    [custombutton addSubview:buttonimageview];
    return custombutton;
}

+(UIButton *)setaddtocartbutton:(NSString *)title
{
    UIButton *custombutton=[[UIButton alloc]initWithFrame:CGRectMake(0, 10, 60, 20)];
    // [custombutton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    custombutton.backgroundColor=[UIColor whiteColor];
//    UIImageView *buttonimageview=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
//    buttonimageview.image=[UIImage imageNamed:@"carticon.png"];
//    buttonimageview.contentMode=UIViewContentModeScaleAspectFit;
//    [custombutton addSubview:buttonimageview];
    [custombutton setTitle:title forState:UIControlStateNormal];
    [custombutton setTitleColor:[UIColor colorWithRed:24/255.0f green:58/255.0f blue:1/255.0f alpha:1.0f] forState:UIControlStateNormal];
//    custombutton.titleLabel.textColor=[UIColor colorWithRed:24/255.0f green:58/255.0f blue:1/255.0f alpha:1.0f];
    (custombutton.titleLabel).font = [UIFont fontWithName:@"Philosopher" size:10.0f];
    custombutton.titleLabel.textAlignment=NSTextAlignmentCenter;
    return custombutton;
}

@end





