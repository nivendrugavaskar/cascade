//
//  MapVC.m
//  CascadeApp
//
//  Created by Nivendru Gavaskar on 14/10/15.
//  Copyright © 2015 Nivendru Gavaskar. All rights reserved.
//

#import "MapVC.h"
#import "AppDelegate.h"

#define METERS_PER_MILE 7000

@interface MapVC ()
{
    AppDelegate *app;
}

@end

@implementation MapVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    app=(AppDelegate *)[UIApplication sharedApplication].delegate;
    // Check for iOS 8. Without this guard the code will crash with "unknown selector" on iOS 7.
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    if(IS_OS_8_OR_LATER)
    {
        [self.locationManager requestAlwaysAuthorization];
        [self.locationManager requestWhenInUseAuthorization];
        if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
            [self.locationManager requestWhenInUseAuthorization];
        }
        
        [self.locationManager startUpdatingLocation];
    }
    else
    {
        [self.locationManager startUpdatingLocation];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma checking allow or not aby user and also GPS is on or not
- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    NSLog(@"Status=%d",status);
    if(status==3)
    {
        
        if([CLLocationManager locationServicesEnabled] &&
           [CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied)
        {
            
        }
        else
        {
            
            
        }
    }
    else
    {
        
        
    }
}

// Location Manager Delegate Methods
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    NSLog(@"Latitude=%f",manager.location.coordinate.latitude);
    NSLog(@"Longitude=%f",manager.location.coordinate.longitude);
    self.myMap.showsUserLocation=YES;
    
    #pragma highlited user location
            CLLocationCoordinate2D zoomLocation;
            zoomLocation=manager.location.coordinate;
            [self.myMap setCenterCoordinate:zoomLocation animated:YES];
            MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 8*METERS_PER_MILE, 8*METERS_PER_MILE);
            [self.myMap setRegion:viewRegion animated:YES];
    
    
    
}
-(MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:(id <MKAnnotation>)annotation
{
    
    MKAnnotationView *pinView = nil;
    if (annotation == self.myMap.userLocation)
    {
        
        static NSString *defaultPinID = @"com.BPD.pin";
        pinView = (MKAnnotationView *)[self.myMap dequeueReusableAnnotationViewWithIdentifier:defaultPinID];
        if ( pinView == nil )
            pinView = [[MKAnnotationView alloc]
                       initWithAnnotation:annotation reuseIdentifier:defaultPinID];
        
        pinView.image=nil;
        pinView.canShowCallout = YES;
        pinView.image=[UIImage imageNamed:@"pin.png"];
        ((MKUserLocation *)annotation).title = @"I am here";
        
//#pragma highlited user location
//        CLLocationCoordinate2D zoomLocation;
//        zoomLocation=annotation.coordinate;
//        [self.myMap setCenterCoordinate:zoomLocation animated:YES];
//        MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 8*METERS_PER_MILE, 8*METERS_PER_MILE);
//        [self.myMap setRegion:viewRegion animated:YES];
//        
//        [self.locationManager stopUpdatingLocation];
        
    }
    else
    {
        
    }
    return pinView;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)didtapLeftMenu:(id)sender {
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}

- (UIStatusBarStyle) preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

@end
