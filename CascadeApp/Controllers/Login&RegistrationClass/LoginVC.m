//
//  ViewController.m
//  CascadeApp
//
//  Created by Nivendru Gavaskar on 13/10/15.
//  Copyright © 2015 Nivendru Gavaskar. All rights reserved.
//

#import "LoginVC.h"
#import "AppDelegate.h"

@interface LoginVC ()

@end

@implementation LoginVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.menuContainerViewController.panMode = MFSideMenuPanModeDefault;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)didTapRegister:(id)sender {
    [self performSegueWithIdentifier:@"segueRegistration" sender:nil];
}
- (IBAction)didTapLogin:(id)sender {
    [self performSegueWithIdentifier:@"seguetoplant" sender:nil];
}
- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end
