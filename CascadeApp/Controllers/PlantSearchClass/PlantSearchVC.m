//
//  PlantSearchVC.m
//  CascadeApp
//
//  Created by Nivendru Gavaskar on 13/10/15.
//  Copyright © 2015 Nivendru Gavaskar. All rights reserved.
//

#import "PlantSearchVC.h"
#import "AppDelegate.h"
#import "PlantSearchVCCell.h"

@interface PlantSearchVC ()<UITableViewDataSource,UITableViewDelegate,CustomIOSAlertViewDelegate>
{
    NSMutableArray *searchArray;
    BOOL isplant;
}

@end

@implementation PlantSearchVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    isplant=FALSE;
    self.menuContainerViewController.panMode = MFSideMenuPanModeDefault;
    searchArray=[[NSMutableArray alloc] initWithObjects:@"Leaves",@"Family",@"Fruits",@"Flowers", nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)didTapLeftMenu:(id)sender {
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}
#pragma tableview datasource and delegate
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PlantSearchVCCell *cell=(PlantSearchVCCell *)[self.tblView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.lbltitle.text=[searchArray objectAtIndex:indexPath.row];
    
    return cell;
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return searchArray.count;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(isplant)
    {
    [self performSegueWithIdentifier:@"pushtoadvancesearch" sender:indexPath];
    }
    else
    {
//        UIAlertController * alert=   [UIAlertController
//                                      alertControllerWithTitle:@"No Plant found?"
//                                      message:@"Would you like to add this plant?"
//                                      preferredStyle:UIAlertControllerStyleAlert];
//        
//        alert.view.tintColor=[UIColor redColor];
//        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault
//                                                   handler:^(UIAlertAction * action)
//                             {
//                                 [self performSegueWithIdentifier:@"seguetoaddplant" sender:nil];
//                             }];
//        
//        UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault
//                                                       handler:^(UIAlertAction * action)
//                                 {
//                                     [alert dismissViewControllerAnimated:YES completion:nil];
//                                 }];
//        
//        
//        [alert addAction:ok];
//        [alert addAction:cancel];
//        [self presentViewController:alert animated:YES completion:nil];
        
        // Here we need to pass a full frame
        CustomIOSAlertView *alertView = [[CustomIOSAlertView alloc] init];
        // Add some custom content to the alert view
        [alertView setContainerView:[self createDemoView]];
        // Modify the parameters
        [alertView setButtonTitles:[NSMutableArray arrayWithObjects:@"No", @"Yes", nil]];
        [alertView setDelegate:self];
        
        // You may use a Block, rather than a delegate.
        [alertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
            NSLog(@"Block: Button at position %d is clicked on alertView %d.", buttonIndex, (int)[alertView tag]);
            [alertView close];
        }];
        
        [alertView setUseMotionEffects:true];
        
        // And launch the dialog
        [alertView show];
    }
}

- (UIView *)createDemoView
{
    UIView *demoView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width-60,60)];
    //demoView.backgroundColor=[UIColor redColor];
    
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 2, demoView.frame.size.width-20,56)];
    //lbl.backgroundColor=[UIColor blueColor];
    lbl.text=@"No Plant found? Would you like to add this plant?";
    lbl.numberOfLines=0;
    lbl.textAlignment=NSTextAlignmentCenter;
//    [imageView setImage:[UIImage imageNamed:@"demo"]];
    [demoView addSubview:lbl];
    
    return demoView;
}
- (void)customIOS7dialogButtonTouchUpInside: (CustomIOSAlertView *)alertView clickedButtonAtIndex: (NSInteger)buttonIndex
{
    NSLog(@"Delegate: Button at position %d is clicked on alertView %d.", (int)buttonIndex, (int)[alertView tag]);
    if(buttonIndex==1)
    {
         [self performSegueWithIdentifier:@"seguetoaddplant" sender:nil];
    }
    [alertView close];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (UIStatusBarStyle) preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

@end
