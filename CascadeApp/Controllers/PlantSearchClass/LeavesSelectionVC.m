//
//  LeavesSelectionVC.m
//  CascadeApp
//
//  Created by Nivendru Gavaskar on 16/10/15.
//  Copyright © 2015 Nivendru Gavaskar. All rights reserved.
//

#import "LeavesSelectionVC.h"
#import "LeavesSelectionVCCell.h"

@interface LeavesSelectionVC ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation LeavesSelectionVC

@synthesize titleArray,imageArray,numberArray;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    titleArray=[[NSMutableArray alloc] initWithObjects:@"Opposite",@"Basal",@"Alternate", nil];
    imageArray=[[NSMutableArray alloc] initWithObjects:@"Opposite_icon.png",@"Basal_icon.png",@"Alternate_icon.png", nil];
    numberArray=[[NSMutableArray alloc] initWithObjects:@"241",@"184",@"312", nil];
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)didTapBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma tableview delegate and datsource
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LeavesSelectionVCCell *cell=(LeavesSelectionVCCell *)[self.tblvw dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.title.text=[titleArray objectAtIndex:indexPath.row];
    cell.imgvw.image=[UIImage imageNamed:[imageArray objectAtIndex:indexPath.row]];
    cell.number.text=[numberArray objectAtIndex:indexPath.row];
    return cell;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.titleArray count];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}


@end
