//
//  PlantSearchVC.h
//  CascadeApp
//
//  Created by Nivendru Gavaskar on 13/10/15.
//  Copyright © 2015 Nivendru Gavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlantSearchVC : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tblView;
@end
