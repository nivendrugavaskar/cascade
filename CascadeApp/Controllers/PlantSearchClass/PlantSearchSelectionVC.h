//
//  PlantSearchSelectionVC.h
//  CascadeApp
//
//  Created by Nivendru Gavaskar on 14/10/15.
//  Copyright © 2015 Nivendru Gavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlantSearchSelectionVC : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tblvw;

@end
