//
//  AddPlantVC.m
//  CascadeApp
//
//  Created by Nivendru Gavaskar on 16/10/15.
//  Copyright © 2015 Nivendru Gavaskar. All rights reserved.
//

#import "AddPlantVC.h"
#import "AppDelegate.h"
#import "AddPlantVCCell.h"

@interface AddPlantVC ()<UICollectionViewDataSource,UICollectionViewDelegate>

@end

@implementation AddPlantVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
#pragma dynamically updating UI
    self.collectionview.frame=CGRectMake(self.collectionview.frame.origin.x, self.collectionview.frame.origin.y, self.collectionview.frame.size.width, (self.collectionview.frame.size.width-30)/4);
    self.scrollvw.frame=CGRectMake(self.scrollvw.frame.origin.x, self.collectionview.frame.origin.y+self.collectionview.frame.size.height+15, self.scrollvw.frame.size.width, self.view.frame.size.height-(self.collectionview.frame.origin.y+self.collectionview.frame.size.height+self.btnuploadplant.frame.size.height+30));
    
    [self.scrollvw setContentSize:CGSizeMake(0, self.innerviewscrllvw.frame.size.height)];
}
- (IBAction)didTapLeftMenu:(id)sender {
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma collection view datasource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 8;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    AddPlantVCCell *cell =(AddPlantVCCell *)[self.collectionview dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    return cell;
    
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    float width;
    width=(self.collectionview.frame.size.width-30)/4;
    return CGSizeMake(width, width);
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}


@end
