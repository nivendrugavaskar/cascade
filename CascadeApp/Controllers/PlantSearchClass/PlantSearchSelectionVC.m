//
//  PlantSearchSelectionVC.m
//  CascadeApp
//
//  Created by Nivendru Gavaskar on 14/10/15.
//  Copyright © 2015 Nivendru Gavaskar. All rights reserved.
//

#import "PlantSearchSelectionVC.h"
#import "AppDelegate.h"
#import "PlantSearchSelectionVCCell.h"

@interface PlantSearchSelectionVC ()<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *leavesArray,*leavesimage;
}

@end

@implementation PlantSearchSelectionVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    leavesArray=[[NSMutableArray alloc] initWithObjects:@"Leaf Arrangment",@"Leaf Shape",@"Leaf Edge", nil];
    leavesimage=[[NSMutableArray alloc] initWithObjects:@"leaf_arrangement_icon.png",@"leaf_shape_icon.png",@"leaf-edge_icon.png", nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma tableview datasource
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PlantSearchSelectionVCCell *cell=(PlantSearchSelectionVCCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.title.text=[leavesArray objectAtIndex:indexPath.row];
    cell.imgvw.image=[UIImage imageNamed:[leavesimage objectAtIndex:indexPath.row]];
    return cell;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return leavesArray.count;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"pushtoleaveselection" sender:indexPath];
}
- (IBAction)didTapBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end
