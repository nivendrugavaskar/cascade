//
//  LeavesSelectionVC.h
//  CascadeApp
//
//  Created by Nivendru Gavaskar on 16/10/15.
//  Copyright © 2015 Nivendru Gavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeavesSelectionVC : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tblvw;

@property NSMutableArray *titleArray,*imageArray,*numberArray;

@end
