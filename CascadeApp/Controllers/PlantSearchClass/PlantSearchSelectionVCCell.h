//
//  PlantSearchSelectionVCCell.h
//  CascadeApp
//
//  Created by Nivendru Gavaskar on 14/10/15.
//  Copyright © 2015 Nivendru Gavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlantSearchSelectionVCCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UIImageView *imgvw;

@end
