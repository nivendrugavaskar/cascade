//
//  PlantSearchTableViewCell.h
//  CascadeApp
//
//  Created by Nivendru Gavaskar on 13/10/15.
//  Copyright © 2015 Nivendru Gavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlantSearchVCCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbltitle;

@end
