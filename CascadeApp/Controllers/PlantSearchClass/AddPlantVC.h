//
//  AddPlantVC.h
//  CascadeApp
//
//  Created by Nivendru Gavaskar on 16/10/15.
//  Copyright © 2015 Nivendru Gavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddPlantVC : UIViewController

@property (weak, nonatomic) IBOutlet UICollectionView *collectionview;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollvw;
@property (weak, nonatomic) IBOutlet UIView *innerviewscrllvw;
@property (weak, nonatomic) IBOutlet UIButton *btnuploadplant;
@end
