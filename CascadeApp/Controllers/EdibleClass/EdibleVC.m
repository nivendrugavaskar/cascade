//
//  EdibleVC.m
//  CascadeApp
//
//  Created by Nivendru Gavaskar on 14/10/15.
//  Copyright © 2015 Nivendru Gavaskar. All rights reserved.
//

#import "EdibleVC.h"
#import "AppDelegate.h"
#import "EdibleVCCell.h"

@interface EdibleVC ()<UITableViewDelegate,UITableViewDataSource>
{
    AppDelegate *app;
}

@end

@implementation EdibleVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    app=(AppDelegate *)[UIApplication sharedApplication].delegate;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma tableview datasource
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    EdibleVCCell *cell=(EdibleVCCell *)[self.tblvw dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    return cell;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;
}
- (IBAction)didTapLeftmenu:(id)sender {
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"pushtoplantdetails" sender:indexPath];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end
