//
//  PlantDetailsVC.m
//  CascadeApp
//
//  Created by Nivendru Gavaskar on 16/10/15.
//  Copyright © 2015 Nivendru Gavaskar. All rights reserved.
//

#import "PlantDetailsVC.h"
#import "PlantDetailsVCCell.h"
#import "AppDelegate.h"

@interface PlantDetailsVC ()<UICollectionViewDataSource,UICollectionViewDelegate,CustomIOSAlertViewDelegate>

@end

@implementation PlantDetailsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
#pragma dynamically updating UI
    self.collectionview.frame=CGRectMake(self.collectionview.frame.origin.x, self.collectionview.frame.origin.y, self.collectionview.frame.size.width, (self.collectionview.frame.size.width-30)/4);
    self.scrollvw.frame=CGRectMake(self.scrollvw.frame.origin.x, self.collectionview.frame.origin.y+self.collectionview.frame.size.height+15, self.scrollvw.frame.size.width, self.view.frame.size.height-(self.collectionview.frame.origin.y+self.collectionview.frame.size.height+30));
    
    [self.scrollvw setContentSize:CGSizeMake(0, self.innerviewscrllvw.frame.size.height)];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)didTapLeftMenu:(id)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}

#pragma collection view datasource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 8;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
        PlantDetailsVCCell *cell =(PlantDetailsVCCell *)[self.collectionview dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
        return cell;
    
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    float width;
    width=(self.collectionview.frame.size.width-30)/4;
    return CGSizeMake(width, width);
}
- (IBAction)didTaplocation:(id)sender {
    
    CustomIOSAlertView *alertView = [[CustomIOSAlertView alloc] init];
    // Add some custom content to the alert view
    self.popview.hidden=FALSE;
    [alertView setContainerView:[self popview]];
    // Modify the parameters
    [alertView setButtonTitles:[NSMutableArray arrayWithObjects:@"Submit", nil]];
    [alertView setDelegate:self];
    
    // You may use a Block, rather than a delegate.
    [alertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
        NSLog(@"Block: Button at position %d is clicked on alertView %d.", buttonIndex, (int)[alertView tag]);
        [alertView close];
    }];
    
    [alertView setUseMotionEffects:true];
    
    // And launch the dialog
    [alertView show];
}

- (UIView *)createDemoView
{
    UIView *demoView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width-60,280)];
    //demoView.backgroundColor=[UIColor redColor];
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, demoView.frame.size.width,30)];
    lbl.backgroundColor=[UIColor colorWithRed:103/255.0f green:203/255.0f blue:200/255.0f alpha:1.0f];
    lbl.text=@"Add Another Location";
    lbl.numberOfLines=0;
    lbl.textAlignment=NSTextAlignmentCenter;
    //    [imageView setImage:[UIImage imageNamed:@"demo"]];
    [demoView addSubview:lbl];
    
    return demoView;
}
- (void)customIOS7dialogButtonTouchUpInside: (CustomIOSAlertView *)alertView clickedButtonAtIndex: (NSInteger)buttonIndex
{
    NSLog(@"Delegate: Button at position %d is clicked on alertView %d.", (int)buttonIndex, (int)[alertView tag]);
    if(buttonIndex==1)
    {
        [self performSegueWithIdentifier:@"seguetoaddplant" sender:nil];
    }
    [alertView close];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
@end
