//
//  SliderTableViewCell.h
//  Baarfi
//
//  Created by Arnab on 17/12/14.
//  Copyright (c) 2014 Arnab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SliderTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *img_Icon;
@property (weak, nonatomic) IBOutlet UILabel *lbl_List;
@property (weak, nonatomic) IBOutlet UIImageView *divider;

@end
