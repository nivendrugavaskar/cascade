//
//  FBSliderMenuViewController.m
//  FBSlider
//  Created by Subhr Roy on 11/02/14.
//  Copyright (c) 2014 Subhr Roy. All rights reserved.
//

#import "FBSliderMenuViewController.h"
#import "MFSideMenu.h"
#import "SliderTableViewCell.h"
#import "LoginVC.h"
#import "EdibleVC.h"
#import "PlantSearchVC.h"
#import "MapVC.h"
#import "GlossaryVC.h"
#import "HelpVC.h"


@interface FBSliderMenuViewController ()
{
    
    NSMutableArray *arraylist;
    NSMutableArray *arrayImage,*arrayHoverImage;
    NSIndexPath *lastindexpath;
    
}

@end

@implementation FBSliderMenuViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


-(void)buttonclick
{
    [self.menuTable deselectRowAtIndexPath:lastindexpath animated:YES];
    SliderTableViewCell *cell=(SliderTableViewCell *)[self.menuTable cellForRowAtIndexPath:lastindexpath];
    cell.contentView.backgroundColor = [UIColor colorWithRed:16/255.0f green:16/255.0f blue:16/255.0f alpha:1.0f];
    cell.lbl_List.textColor=[UIColor whiteColor];
    cell.img_Icon.image=arrayImage[lastindexpath.row];
    
    NSIndexPath *indexPath=[NSIndexPath indexPathForRow:2 inSection:0];
    
    if(indexPath.row==6)
    {
        [self.menuTable deselectRowAtIndexPath:indexPath animated:YES];
    }
    
    if(!lastindexpath)
    {
        lastindexpath=0;
    }
    
    if (![indexPath compare:lastindexpath] == NSOrderedSame)
    {
        if(indexPath.row==6)
        {
            [self.menuTable cellForRowAtIndexPath:indexPath].contentView.backgroundColor = [UIColor colorWithRed:16/255.0f green:16/255.0f blue:16/255.0f alpha:1.0f];
            NSIndexPath *ind=[NSIndexPath indexPathForRow:0 inSection:0];
            [self.menuTable cellForRowAtIndexPath:ind].contentView.backgroundColor = [UIColor colorWithRed:30/255.0f green:30/255.0f blue:30/255.0f alpha:1.0f];
            SliderTableViewCell *cell=(SliderTableViewCell *)[self.menuTable cellForRowAtIndexPath:ind];
            cell.lbl_List.textColor=[UIColor whiteColor];
            cell.img_Icon.image=arrayHoverImage[ind.row];
            
        }
        else
        {
            [self.menuTable cellForRowAtIndexPath:lastindexpath].contentView.backgroundColor = [UIColor colorWithRed:16/255.0f green:16/255.0f blue:16/255.0f alpha:1.0f];
            SliderTableViewCell *cell=(SliderTableViewCell *)[self.menuTable cellForRowAtIndexPath:lastindexpath];
            cell.lbl_List.textColor=[UIColor whiteColor];
            cell.img_Icon.image=arrayImage[lastindexpath.row];
        }
        
        
    }
#pragma maintain on click logout
    if(indexPath.row==6)
    {
        [self.menuTable cellForRowAtIndexPath:indexPath].contentView.backgroundColor = [UIColor colorWithRed:16/255.0f green:16/255.0f blue:16/255.0f alpha:1.0f];
        SliderTableViewCell *cell=(SliderTableViewCell *)[self.menuTable cellForRowAtIndexPath:indexPath];
        cell.lbl_List.textColor=[UIColor whiteColor];
        cell.img_Icon.image=arrayImage[indexPath.row];
        lastindexpath=[NSIndexPath indexPathForRow:0 inSection:0];
        
    }
    else
    {
        [self.menuTable cellForRowAtIndexPath:indexPath].contentView.backgroundColor = [UIColor colorWithRed:30/255.0f green:30/255.0f blue:30/255.0f alpha:1.0f];
        SliderTableViewCell *cell=(SliderTableViewCell *)[self.menuTable cellForRowAtIndexPath:indexPath];
        cell.lbl_List.textColor=[UIColor whiteColor];
        lastindexpath=indexPath;
        cell.img_Icon.image=arrayHoverImage[indexPath.row];
    }
    //[tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self  selectViewControllerWithIndex:(int)indexPath.row];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    app=(AppDelegate*)[UIApplication sharedApplication].delegate;
    
   
    
    self.navigationController.navigationBarHidden=YES;
    
    arraylist=[[NSMutableArray alloc] initWithObjects:@"Plant Search",@"Map",@"Glossary",@"Vegetation Communities",@"Edible Quick Browse",@"Help",@"Logout",nil];
    
    arrayImage=[[NSMutableArray alloc]initWithObjects:[UIImage imageNamed:@"plant-search_icon.png"],[UIImage imageNamed:@"map_icon.png"],[UIImage imageNamed:@"Glossary_icon.png"],[UIImage imageNamed:@"Vegetation-Communities_icon.png"],[UIImage imageNamed:@"Edible-Medicinal-Quick-Browse_icon.png"],[UIImage imageNamed:@"help_icon.png"],[UIImage imageNamed:@"logout_icon.png"],nil];
    
    arrayHoverImage=[[NSMutableArray alloc]initWithObjects:[UIImage imageNamed:@"plant-search_hover_icon.png"],[UIImage imageNamed:@"map_hover_icon.png"],[UIImage imageNamed:@"Glossary_hover_icon.png"],[UIImage imageNamed:@"Vegetation-Communities_hover_icon.png"],[UIImage imageNamed:@"Edible-Medicinal-Quick-Browse_hover_icon.png"],[UIImage imageNamed:@"help_hover_icon.png"],[UIImage imageNamed:@"logout_hover_icon.png"],nil];
    
    
    

}

-(void)selectcart:(NSNotification *)notification
{
    if ([notification.name isEqualToString:@"cartpage"]) {
        NSLog(@"Got notificaton");
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
   [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(selectcart:) name:@"cartpage" object:self];
}

#pragma mark -
#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return arraylist.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row==7)
    {
        return 1000;
    }
    return 52;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellNameFirst = @"listCell";
    
    SliderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellNameFirst];
    
    
    if (cell == NULL)
    {
        cell = [[SliderTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellNameFirst];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if(!lastindexpath)
    {
        if(indexPath.row==0)
        {
            
            cell.contentView.backgroundColor = [UIColor colorWithRed:30/255.0f green:30/255.0f blue:30/255.0f alpha:1.0f];
            cell.lbl_List.textColor=[UIColor whiteColor];
            cell.img_Icon.image=arrayHoverImage[indexPath.row];;
            lastindexpath=indexPath;
            
        }
    }
//    UIView *selected = [[UIView alloc]initWithFrame:cell.frame];
//    [selected setBackgroundColor:[UIColor lightGrayColor]];
//    cell.selectedBackgroundView =  selected;
  
     cell.contentView.backgroundColor=[UIColor colorWithRed:16/255.0f green:16/255.0f blue:16/255.0f alpha:1.0f];
    cell.lbl_List.text=arraylist[indexPath.row];
    cell.img_Icon.image=arrayImage[indexPath.row];
    cell.divider.backgroundColor=[UIColor lightGrayColor];
    return cell;
}


#pragma mark -
#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    //NSLog(@"displaycell=%ld",indexPath.row);
    SliderTableViewCell *cell1=(SliderTableViewCell *)cell;
    if(lastindexpath.row==indexPath.row)
    {
        cell1.contentView.backgroundColor = [UIColor colorWithRed:30/255.0f green:30/255.0f blue:30/255.0f alpha:1.0f];
        cell1.lbl_List.textColor=[UIColor whiteColor];
         cell1.img_Icon.image=arrayHoverImage[indexPath.row];
        cell1.divider.backgroundColor=[UIColor lightGrayColor];
        
    }
    else
    {
        
        cell.contentView.backgroundColor=[UIColor colorWithRed:16/255.0f green:16/255.0f blue:16/255.0f alpha:1.0f];
        cell1.lbl_List.textColor=[UIColor whiteColor];
         cell1.img_Icon.image=arrayImage[indexPath.row];
        cell1.divider.backgroundColor=[UIColor lightGrayColor];
    }
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
   // NSLog(@"last index path=%ld",(long)lastindexpath.row);
    if(indexPath.row==6)
    {
     [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
    
    if(!lastindexpath)
    {
        lastindexpath=0;
    }
    
    if (![indexPath compare:lastindexpath] == NSOrderedSame)
    {
        if(indexPath.row==6)
        {
            [tableView cellForRowAtIndexPath:indexPath].contentView.backgroundColor = [UIColor colorWithRed:16/255.0f green:16/255.0f blue:16/255.0f alpha:1.0f];
            NSIndexPath *ind=[NSIndexPath indexPathForRow:0 inSection:0];
            [tableView cellForRowAtIndexPath:ind].contentView.backgroundColor = [UIColor colorWithRed:30/255.0f green:30/255.0f blue:30/255.0f alpha:1.0f];
            SliderTableViewCell *cell=(SliderTableViewCell *)[tableView cellForRowAtIndexPath:ind];
            cell.lbl_List.textColor=[UIColor whiteColor];
            cell.img_Icon.image=arrayHoverImage[ind.row];
            cell.divider.backgroundColor=[UIColor lightGrayColor];

        }
        else
        {
            [tableView cellForRowAtIndexPath:lastindexpath].contentView.backgroundColor = [UIColor colorWithRed:16/255.0f green:16/255.0f blue:16/255.0f alpha:1.0f];
            SliderTableViewCell *cell=(SliderTableViewCell *)[tableView cellForRowAtIndexPath:lastindexpath];
            cell.lbl_List.textColor=[UIColor whiteColor];
             cell.img_Icon.image=arrayImage[lastindexpath.row];
            cell.divider.backgroundColor=[UIColor lightGrayColor];
        }
       
        
    }
#pragma maintain on click logout
    if(indexPath.row==6)
    {
       [tableView cellForRowAtIndexPath:indexPath].contentView.backgroundColor = [UIColor colorWithRed:16/255.0f green:16/255.0f blue:16/255.0f alpha:1.0f];
        SliderTableViewCell *cell=(SliderTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
        cell.lbl_List.textColor=[UIColor whiteColor];
        cell.img_Icon.image=arrayImage[indexPath.row];
        lastindexpath=[NSIndexPath indexPathForRow:0 inSection:0];
        cell.divider.backgroundColor=[UIColor lightGrayColor];
        
    }
    else
    {
      [tableView cellForRowAtIndexPath:indexPath].contentView.backgroundColor = [UIColor colorWithRed:30/255.0f green:30/255.0f blue:30/255.0f alpha:1.0f];
        SliderTableViewCell *cell=(SliderTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
        cell.lbl_List.textColor=[UIColor whiteColor];
        lastindexpath=indexPath;
        cell.img_Icon.image=arrayHoverImage[indexPath.row];
        cell.divider.backgroundColor=[UIColor lightGrayColor];
    }
    //[tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self  selectViewControllerWithIndex:(int)indexPath.row];
   
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //NSLog(@"deselect index path=%ld",(long)indexPath.row);
    SliderTableViewCell *cell=(SliderTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
      cell.contentView.backgroundColor=[UIColor colorWithRed:16/255.0f green:16/255.0f blue:16/255.0f alpha:1.0f];
    cell.lbl_List.textColor=[UIColor whiteColor];
    cell.img_Icon.image=arrayImage[indexPath.row];
    cell.divider.backgroundColor=[UIColor lightGrayColor];
    
    
    
}


-(void)selectViewControllerWithIndex:(int)_index
{
    switch (_index) {
           
                    case 0:
                    {
                       
                        PlantSearchVC *plantvc = [self.storyboard instantiateViewControllerWithIdentifier:@"PlantSearchVC"];
                        UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
                        NSArray *controllers = @[plantvc];
                        navigationController.viewControllers = controllers;
                        (self.menuContainerViewController).menuState = MFSideMenuStateClosed;
                        }
                                    break;
            
                    case 1:
                        {
                            MapVC *mapvc = [self.storyboard instantiateViewControllerWithIdentifier:@"MapVC"];
                            UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
                            NSArray *controllers = @[mapvc];
                            navigationController.viewControllers = controllers;
                            (self.menuContainerViewController).menuState = MFSideMenuStateClosed;
                        }
           
                                break;
                    case 2:
                        {
                            GlossaryVC *glvc = [self.storyboard instantiateViewControllerWithIdentifier:@"GlossaryVC"];
                            UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
                            NSArray *controllers = @[glvc];
                            navigationController.viewControllers = controllers;
                            (self.menuContainerViewController).menuState = MFSideMenuStateClosed;
                           
                        }
                            break;
            
                    case 3:
                        {
            
                       
                        }
            
                        break;
                    case 4:
                        {
                            EdibleVC *ediblevc = [self.storyboard instantiateViewControllerWithIdentifier:@"EdibleVC"];
                            UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
                            NSArray *controllers = @[ediblevc];
                            navigationController.viewControllers = controllers;
                            (self.menuContainerViewController).menuState = MFSideMenuStateClosed;
                        }
                        break;
                    case 5:
                        {
                            HelpVC *helpvc = [self.storyboard instantiateViewControllerWithIdentifier:@"HelpVC"];
                            UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
                            NSArray *controllers = @[helpvc];
                            navigationController.viewControllers = controllers;
                            (self.menuContainerViewController).menuState = MFSideMenuStateClosed;
                        }
                            break;
                   case 6:
                    {
                        
                        LoginVC *loginViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                        UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
                        NSArray *controllers = @[loginViewController];
                        navigationController.viewControllers = controllers;
                        (self.menuContainerViewController).menuState = MFSideMenuStateClosed;
                    }
                            break;
                   case 7:
                    {
                       
                    }
                    default:
                    break;
    }

}

@end
