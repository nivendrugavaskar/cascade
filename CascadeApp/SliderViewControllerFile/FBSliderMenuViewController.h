//
//  FBSliderMenuViewController.h
//  FBSlider
//
//  Created by Subhr Roy on 11/02/14.
//  Copyright (c) 2014 Subhr Roy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"


@interface FBSliderMenuViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    AppDelegate *app;
}


@property (weak, nonatomic) IBOutlet UITableView *menuTable;


-(void)selectViewControllerWithIndex:(int)_index;


@end
